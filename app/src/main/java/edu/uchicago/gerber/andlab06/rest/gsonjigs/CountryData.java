package edu.uchicago.gerber.andlab06.rest.gsonjigs;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class CountryData  {

    @SerializedName("RestResponse")
    @Expose
    private edu.uchicago.gerber.andlab06.rest.gsonjigs.RestResponse RestResponse;

    /**
     *
     * @return
     * The RestResponse
     */
    public edu.uchicago.gerber.andlab06.rest.gsonjigs.RestResponse getRestResponse() {
        return RestResponse;
    }

    /**
     *
     * @param RestResponse
     * The RestResponse
     */
    public void setRestResponse(edu.uchicago.gerber.andlab06.rest.gsonjigs.RestResponse RestResponse) {
        this.RestResponse = RestResponse;
    }

}