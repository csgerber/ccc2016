// ContactsFragment.java
// Fragment subclass that displays the alphabetical list of contact names
package edu.uchicago.gerber.andlab06.frags;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.firebase.ui.FirebaseListAdapter;

import edu.uchicago.gerber.andlab06.MyCountry;
import edu.uchicago.gerber.andlab06.R;

public class CountriesFragment extends Fragment {



    private static final int CONTACTS_LOADER = 0; // identifies Loader


    // private CountriesAdapter contactsAdapter; // adapter for recyclerView

    private FirebaseListAdapter<MyCountry> mAdapter;
    private Firebase dbReference;
    private ListView lvCountries;

    // configures this fragment's GUI
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true); // fragment has menu items to display

        // inflate GUI and get reference to the RecyclerView
        View view = inflater.inflate(
                R.layout.fragment_contacts, container, false);


        dbReference = new Firebase("https://andlab10c.firebaseio.com");
        Firebase messagesRef = dbReference.child("MyCountry");
        messagesRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                refreshData();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        lvCountries = (ListView) view.findViewById(R.id.lvCountries);


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshData();
    }


    private void refreshData() {
        Firebase messagesRef = dbReference.child("MyCountry");
        mAdapter = new FirebaseListAdapter<MyCountry>(

                getActivity(),
                MyCountry.class,
                R.layout.reminders_row,
                messagesRef


        ) {
            @Override
            protected void populateView(View view, MyCountry s, int i) {


                View list_tab = view.findViewById(R.id.row_tab);
                if (s.isFav())
                    list_tab.setBackgroundResource(R.color.orange);
                else
                    list_tab.setBackgroundResource(R.color.green);

                TextView textView = (TextView) view.findViewById(R.id.row_text);
                ImageView img = (ImageView) view.findViewById(R.id.list_image);
                textView.setText(s.getName());

                Glide.with(getActivity())
                        .load(s.getUrl())
                        .placeholder(R.drawable.ic_google)
                        .into(img);
            }


        };


        lvCountries.setAdapter(mAdapter);

        lvCountries.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MyCountry myCountry = (MyCountry) lvCountries.getItemAtPosition(position);
                Firebase itemRef = mAdapter.getRef(position);
                itemRef.child("fav").setValue(!myCountry.isFav());

            }
        });

        lvCountries.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Firebase itemRef = mAdapter.getRef(position);
                itemRef.removeValue();
                return true;
            }
        });


    }
}


